const createNewUser = () => {
    const firstName = prompt('Enter your first name');
    const lastName = prompt('Enter your last name');

    return newUser = {
        firstName,
        lastName,

        getLogin() {
            return login = (this.firstName.slice(0, 1) + this.lastName).toLowerCase();
        },

        getAge() {
            let age = '';
            let birthday = (prompt('Enter your date of birth', 'dd.mm.yyyy')).replaceAll('.', '');
            let day = birthday.slice(0, 2);
            let month = birthday.slice(2, 4);
            let year = birthday.slice(4);
            this.birthday = new Date(`${year}-${month}-${day}`);
            age = (new Date() - new Date(this.birthday)) / (1000 * 60 * 60 * 24 * 365.25);
            return Math.trunc(age);
        },

        getPassword() {
            return password = `${this.firstName.slice(0, 1).toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`;
        },
    };
};

createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

/*

"use strict";

const createNewUser = () => {
    
    let firstName = prompt("What is your name?")
    let lastName = prompt("what is your last name?")
    let birthday = prompt(`"What is your birthday? dd.mm.yyyy`)
    
    const newUser = {
        firstName,
        lastName,
        
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        birthday,
        
        getAge() {
            const year_in_ms = 1000 * 60 * 60 * 24 * 365; // ms * sec * min * hours * days
            return Math.floor((new Date() - new Date(birthday)) / year_in_ms)
        },
        
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6)
        }
    }
    return newUser
}

const user = createNewUser()

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

*/
//////////////////////////////////////////////////////////////////
/*

"use strict"


let firstName = prompt('Як вас звати ?');
let lastName = prompt('Яке ваше прізвище ?');
let birthday = prompt('Запиши свою дату народження у вигляді m.d.y');

let createNewUser = (firstName, lastName, birthday) => {

  const newUser = {
    firstName,
    lastName,
    birthday,

    getLogin () {

      return this.firstName.slice(0, 1).toLocaleLowerCase() + this.lastName.toLocaleLowerCase();

    },

    getYear () {

      let parts = this.birthday.split(".");
      let day = parts[0];
      let month = parts[1];
      let yearNew = parts[2];
      let date = new Date(yearNew, month - 1, day);
      let dataNow = new Date();
 
      
      let year = dataNow.getFullYear() - date.getFullYear();

      date.setFullYear(dataNow.getFullYear());

      if(dataNow < date){

        year--

      }

      this.birthday = year;  

      return this.birthday;

  },

  getPassword () {

    return this.firstName.slice(0,1).toUpperCase() + this.lastName.toLocaleLowerCase() + this.birthday;

  },

}

    return newUser;

}

const user1 = createNewUser(firstName,lastName, birthday);
console.log(user1.getLogin());
console.log(user1.getYear());
console.log(user1.getPassword());

*/

///////////////////////////////////////////////////////////////
/*
1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування:

 Екранування потрібно для того, щоб використати спеціальний символ, як звичайний.
 Наприклад const name = 'Ім\'я'.

 2.Які засоби оголошення функцій ви знаєте?

 1 - Оголошення за допомогою ключового слова function
 2 - Функціональний вираз або анонімна функція
 3 - Стрілкова функція
 4 - Функції-конструктори
 5 - Функції, що повертають іншу функцію

 3.Що таке hoisting, як він працює для змінних та функцій?

 Hoisting - це механізм в JavaScript, за яким усі оголошення змінних та функцій переміщуються вверх до початку контексту виконання.
 Це означає, що змінні та функції можна використовувати в коді до їх фактичного оголошення.
 */
/////////////////////////////////////////////////////////////////